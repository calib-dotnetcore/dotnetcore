﻿using Microsoft.EntityFrameworkCore;
using System;

namespace SqlServerDBContext
{
    public class SqlServerDbContext:DbContext
    {
        public SqlServerDbContext(DbContextOptions options) : base(options)
        { }
    }
}
