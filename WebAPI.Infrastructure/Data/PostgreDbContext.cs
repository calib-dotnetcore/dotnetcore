﻿using Microsoft.EntityFrameworkCore;

namespace WebAPI.Infrastructure.Data
{
    public class PostgreDbContext : PostgresDBContext.PostgresDbContext
    {
       public PostgreDbContext(DbContextOptions options): base(options)
        { }
    }
}
