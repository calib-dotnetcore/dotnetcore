﻿namespace WebAPI.Core.Entities.Base
{
    public abstract class EntityBase<TId> : IEntityBase<TId>
    {
        public virtual TId Id { get; protected set; }

        private int? _requestedHashCode;
        /// <summary>
        /// To find whether the record is new or existing
        /// </summary>
        /// <returns>returns true if it is new record</returns>
        public bool IsTransient => Id.Equals(default(TId));

        public override bool Equals(object obj)
        {
            if (!(obj is EntityBase<TId>))
                return false;

            if (ReferenceEquals(this, obj))
                return true;

            if (GetType() != obj.GetType())
                return false;

            var item = (EntityBase<TId>)obj;

            return !item.IsTransient&& !IsTransient && item == this;
        }
        /// <summary>
        /// To generate the new Hashcode
        /// </summary>
        /// <returns>Hashcode</returns>
        public override int GetHashCode()
        {
            if (!IsTransient)
            {
                if (!_requestedHashCode.HasValue)
                    _requestedHashCode = Id.GetHashCode() ^ 31; // XOR for random distribution (http://blogs.msdn.com/b/ericlippert/archive/2011/02/28/guidelines-and-rules-for-gethashcode.aspx)

                return _requestedHashCode.Value;
            }
            else
                return base.GetHashCode();
        }

        public static bool operator ==(EntityBase<TId> left, EntityBase<TId> right) => Equals(left, null) ? Equals(right, null) : left.Equals(right);

        public static bool operator !=(EntityBase<TId> left, EntityBase<TId> right) => !(left == right);
    }
}
