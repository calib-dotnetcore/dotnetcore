﻿using Microsoft.EntityFrameworkCore;

namespace PostgresDBContext
{
    public class PostgresDbContext : DbContext
    {
        public PostgresDbContext(DbContextOptions options) : base(options)
        { }
    }
}
